//
//  ViewController.swift
//  CRUD
//
//  Created by lw-mini-13 on 11/12/19.
//  Copyright © 2019 lw-mini-13. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - VARIABLE DECLARATION

    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtNumber : UITextField!
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtCpassword : UITextField!
    @IBOutlet var imgImage : UIImageView!
    @IBOutlet var btnAdd : UIButton!

    var name : String = ""
    var phno : String = ""
    var email : String = ""
    var pass : String = ""
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let phoneRegEx = "^[6-9][0-9]{9}$"

    	
    var imageV : NSData?
    
    //MARK: - ON LOAD

    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.text = name
        btnAdd.setTitle("Update", for: .normal)
        if txtName.text == "" {
            txtName.isEnabled = true
            txtPassword.isEnabled = true
            btnAdd.setTitle("Add", for: .normal)
        }
        txtNumber.text = phno
        txtEmail.text = email
        txtPassword.text = pass
        if imageV != nil{
            imgImage.image = UIImage(data: imageV! as Data)
        }
    }
    //MARK: - FUNC METHODS

//    func  creatData() {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
//
//        let manageContact = appDelegate.persistentContainer.viewContext
//
//        let userEntity = NSEntityDescription.entity(forEntityName: kEntityStr, in: manageContact)!
//
//        let user = NSManagedObject(entity: userEntity, insertInto: manageContact)
//
//        user.setValue(txtName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kNameStr)
//        user.setValue(txtNumber.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kPhoneStr)
//        user.setValue(txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kEmailStr)
//        user.setValue(txtCpassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kPassStr)
//        user.setValue(imageV, forKey: kImageStr)
//        do{
//            try manageContact.save()
//                print("save successful...")
//
//            }catch let error as NSError{
//                print(error)
//        }
//    }
    
    func  updateData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let manageContact = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: kEntityStr, in: manageContact)!
        let fatchRequest : NSFetchRequest<NSFetchRequestResult> =  NSFetchRequest.init(entityName: kEntityStr)
        
        let sort = NSSortDescriptor(key: kIdStr, ascending: true)
        fatchRequest.sortDescriptors = [sort]
        
        fatchRequest.predicate = NSPredicate(format: "name = %@", txtName.text!)
            do{
                let data = try manageContact.fetch(fatchRequest)
                if data.count>0{
                    let newData = data[0] as! NSManagedObject
//                    print(newData)
                    newData.setValue(txtName.text, forKey: kNameStr)
                    newData.setValue(txtNumber.text, forKey: kPhoneStr)
                    newData.setValue(txtEmail.text, forKey: kEmailStr)
                    newData.setValue(txtCpassword.text, forKey: kPassStr)
                    newData.setValue(imageV, forKey: kImageStr)
                    do{
                        try manageContact.save()
                        self.view.makeToast("Update successful...")
                    } catch{
                        print(error)
                    }
                }else{
                    let user = NSManagedObject(entity: userEntity, insertInto: manageContact)
                    let count = try? manageContact.count(for: NSFetchRequest.init(entityName: kEntityStr))
                    user.setValue((count!), forKey: kIdStr)
                    user.setValue(txtName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kNameStr)
                    user.setValue(txtNumber.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kPhoneStr)
                    user.setValue(txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kEmailStr)
                    user.setValue(txtCpassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), forKeyPath: kPassStr)
                    user.setValue(imageV, forKey: kImageStr)
                    print("user id is " + "\(String(describing: count))")
                    do{
                        try manageContact.save()
                            self.view.makeToast("save successful...")
                        
                        }catch let error as NSError{
                            print(error)
                          }
                }
            }catch{
                print(error)
            }
    }
    func  deleteData() {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
            let manageContact = appDelegate.persistentContainer.viewContext
            let fatchRequest : NSFetchRequest<NSFetchRequestResult> =  NSFetchRequest.init(entityName: kEntityStr)
            fatchRequest.predicate = NSPredicate(format: "name = %@", txtName.text!)
                do{
                    let data = try manageContact.fetch(fatchRequest)
                    if data.count>0{
                        let newData = data[0] as! NSManagedObject
                        manageContact.delete(newData)
                        do{
                            try manageContact.save()
                            self.view.makeToast("Delete successful...")
                        } catch{
                            print(error)
                        }
                    }else{
                    self.view.makeToast("No Data Available..")
                    }
                }catch{
                    print(error)
                }
            }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = txtNumber!.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
//        return txtNumber.text <= 10 || String == ""
    }
    
    //MARK: - CLICK EVENT
    
    @IBAction func btnAdd(_ sender :UIButton){
        self.view.endEditing(true)

    
       
        let phonePred = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
               
               if !(phonePred.evaluate(with: (txtNumber.text)!)) {
                   self.view.makeToast("enter valid Mobile Number.")
                   return
               }
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            
            if !(emailPred.evaluate(with: (txtEmail.text)!)) {
                self.view.makeToast("enter valid email id.")
                return
            }
        if txtName.text == "" || txtNumber.text == "" || txtEmail.text == "" || txtPassword.text == ""   || txtPassword.text != txtCpassword.text{
            self.view.makeToast("All Field Required...")
            return
        }
        updateData()
    }
//    @IBAction func btnUpdate (_ sender : UIButton){
//        if txtName.text == "" || txtNumber.text == ""{
//            return
//        }
//        updateData()
//        sender.setTitle("DONE", for: .normal)
//    }
    @IBAction func btnDelete(_ sender :UIButton){
        self.view.endEditing(true)

        if txtName.text == ""{
            return
        }
        deleteData()
    }
    @IBAction func imgImage(_ sender : UIButton){
        self.view.endEditing(true)
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        self.present(image,animated: true)
        {
            
        }
    }

//    MARK: - DELEGATE METHODS
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imagee = info[.editedImage] as? UIImage
        {
            imgImage.image = imagee
            imageV = imagee.jpegData(compressionQuality: 0.7) as NSData?
        }
        else{
            print("Error...")
        }
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
