//
//  ViewController.swift
//  CRUD
//
//  Created by lw-mini-13 on 11/12/19.
//  Copyright © 2019 lw-mini-13. All rights reserved.
//

import UIKit
import CoreData

class RowCell : UITableViewCell{
    @IBOutlet var lblname : UILabel!
    @IBOutlet var lblnumber : UILabel!
    @IBOutlet var imgImage : UIImageView!
}

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet var tblContact : UITableView!
    var contact :[NSManagedObject] = []
    func reciveData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let manageContact = appDelegate.persistentContainer.viewContext

        let fatchRequese = NSFetchRequest<NSFetchRequestResult>(entityName: kEntityStr)
        
        do{
            let result = try manageContact.fetch(fatchRequese)
            for data in result as! [NSManagedObject]{
                print(data.value(forKey: kNameStr) as! String)
                print(data.value(forKey: kPhoneStr) as! String)
            }
        }catch{
            print("failed")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        reciveData()
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
            
        }
                 
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: kEntityStr)
        
        //3
        do {
            contact = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        self.tblContact.reloadData()
    }
    

    @IBAction func btnAdd(_sender : UIButton){
        let AddContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(AddContact_VC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contact.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rowcell", for: indexPath) as! RowCell
        let person = contact[indexPath.row]
        cell.lblname.text = person.value(forKey: kNameStr) as? String
        cell.lblnumber.text = person.value(forKey: kPhoneStr) as? String
        if person.value(forKey: kImageStr) != nil{
            let imageData : NSData? = (person.value(forKey: kImageStr) as? NSData)!
            cell.imgImage.image = UIImage(data: imageData! as Data)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let username = contact[indexPath.row]

        let AddContact_VC = self.storyboard?.instantiateViewController(withIdentifier:"ViewController") as! ViewController
        AddContact_VC.name = username.value(forKey: kNameStr) as! String
        AddContact_VC.phno = username.value(forKey: kPhoneStr) as! String
        AddContact_VC.email = username.value(forKey: kEmailStr) as! String
        AddContact_VC.pass = username.value(forKey: kPassStr) as! String

        AddContact_VC.imageV = username.value(forKey: kImageStr) as? NSData
        print(username)
        self.navigationController?.pushViewController(AddContact_VC, animated: true)
    }
      
     
}

