//
//  TPKeyboardAvoidingScrollView.h
//  TPKeyboardAvoiding
//
//  Created by Michael Tyson on 30/09/2013.
//  Copyright 2015 A Tasty Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"

@protocol CommonDelegate <NSObject>

@optional
-(BOOL)shouldChangeTextViewText:(id) textView range: (NSRange) range1 text:(NSString *)str1;
-(BOOL)textFieldDidBeginEdit:(UITextField*)textField;
-(void)textFieldDidEndEdit:(id)textField;
@end


@interface TPKeyboardAvoidingScrollView : UIScrollView <UITextFieldDelegate, UITextViewDelegate>
{
    
}
@property(nonatomic,weak) id<CommonDelegate> mydelegate;

- (void)contentSizeToFit;
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
